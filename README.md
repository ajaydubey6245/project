<!DOCTYPE html>
<html>
    <head>

        <script src="https://cdn.jsdelivr.net/npm/chart.js@3.8.2/dist/chart.min.js"></script>

        <title>
            IITR_Task2
        </title>

        <style>

            .dis_1{
                border-style: solid;
                border-width:5px;
                border-color: #F0E2BF;
                width: 50px;
                height: 20px;
            }

            .btn{
                border-style: solid;
                border-color: #000000;
                border-width: 5px;
                background-color: #9AD9EA;
                width: 120px;
                height: 30px;
                padding-left: 20px;
                text-align: left;
                margin-left: 120px;
                margin-bottom: 1px;
                margin-top: 50px;
                font-size: 20px;
            }

            .btn:hover{
                background-color: #3b87b7;
            }

            .inps{
                margin-left: 130px;
                width: 270px;
            }

            .points{
                border-style: solid;
                border-width: 3px;
                border-color: black;
                background-color: white;
                font-size: 20px;
                font-family: arial;
                width: 200px;
                border-collapse: collapse;
                text-align: left;
            }

            .tr, td{
                border-style: solid;
                border-color: black;
                border-width: 3px;
            }

            .entry{
                border-style: hidden;
                height: 20px;
            }

            .mygraph{
                max-width:500px;
                max-height: 300px;
                background-color: white;
                border: 3px solid;

                position: absolute;
                left:750px;
                top:300px;
            }

        </style>

    </head>

    <body style="background-color:#fa80a9;">

        <h1 style="text-align:center; font-family:arial"> Task 2 </h1>

        <div class="inps">

            <label for="x_slider" style="font-style: ariel; font-size: 20px;" > x </label>
            <input type="range" min="1" max="100" name="x_slider" id="x_slider" step="0.001" style="margin-left:20px; margin-right:20px;">
            <input type="text" class="dis_1" id="show_x" value="50"><br><br>

            <label for="x_slider" style="font-style: ariel; font-size: 20px ;" > y </label>
            <input type="range" min="1" max="10" name="y_slider" id="y_slider" step="0.001" style="margin-left:20px; margin-right:20px;">
            <input type="text" class="dis_1" id="show_y" value="5.5"><br>
        </div>

        <div>
            <button class="btn" id="b1">ADD</button>

            <button class="btn" id="b2" style="margin-left:570px">PLOT</button>
        </div>

        <canvas id="plot" class="mygraph">


        </canvas>

        <div style="margin-top: 40px; margin-left: 120px">
            <table class="points" id="table1">
                <tr>
                    <td style="padding-left:10px;"> x </td>
                    <td style="padding-left:10px;"> y </td>
                    <td style="padding-left:10px;"> log x </td>
                </tr>

            </table>
        </div>


        <script>

            var x = document.getElementById('x_slider');
            var sx = document.getElementById('show_x');

            var y = document.getElementById('y_slider');
            var sy = document.getElementById('show_y');

            var table = document.getElementById('table1');

            var c = document.getElementById('plot')
            var b1 = document.getElementById('b1');
            var b2 = document.getElementById('b2');

            x.oninput = function val_x(){
                sx.value=this.value;
            }

            y.oninput = function val_y(){
                sy.value=this.value;
            }
